Ext.define('myApp.controller.MyController', {
    extend : 'Ext.app.Controller',

    init : function() {
        this.control({

            'panel' : {
                render : this.onPanelRendered
            },
            'panel button' : {
                click : this.onButtonClick
            }
        });
    },

    onPanelRendered : function() {
        //console.log('The container was rendered');
    },

    onButtonClick : function(button) {
        //console.log('Button Click');
        var myView = button.up('panel');
        var productDescription = myView.getComponent('productDescription');
        var myEditor = myView.getComponent('myEditor');
        if(button.getText() === "Edit"){
            button.setText("Save");
            myEditor.setValue(productDescription.getValue());
            productDescription.hide();
            myEditor.show();
        }
        else {
            button.setText("Edit");
            productDescription.setValue(myEditor.getValue());
            myEditor.hide();
            productDescription.show();
        }

    }

});