# -*- coding: UTF-8 -*-
from django import forms
import models

CATEGORY_CHOISES = ((c.caption,c.id) for c in models.Category.objects.all())


class EditForm(forms.Form):
    caption = forms.CharField(label="Заголовок")
    text = forms.CharField(label="Текст заметки")
    category = forms.ChoiceField(label="Категория",choices=CATEGORY_CHOISES)
    favorite = forms.BooleanField(required=False)
    published = forms.BooleanField(required=False)
    uuid = forms.CharField(required=False)
    id = forms.CharField(required=False)
