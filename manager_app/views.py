# -*- coding: UTF-8 -*-
from django.shortcuts import get_object_or_404  # render_to_response
# from django.template.context import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
# from django.contrib.auth import logout
# from django.core import serializers
import datetime
import json
import time
# import sys
import models
import uuid
from time import strftime
# import re


@login_required(login_url='/accounts/login/')
def home(request):
    return render(request, 'base.html')


@login_required(login_url='/accounts/login/')
def notes(request):
    c = {}
    print "START"
    print request.GET['start']
    print "LIMIT"
    print request.GET['limit']
    #Получение параметров для выборки
    start = int(request.GET['start'])
    limit = int(request.GET['limit'])

    #Подсчет общего числа записей пользователя
    '''note_count = models.Note.objects.filter(
        user=request.user
    ).count()'''

    #Выборка заметок, начиная со start до (start+limit)
    notes_list = models.Note.objects.filter(
        user=request.user
    )

    if 'filter' in request.GET:
        filters = json.loads(request.GET['filter'])
        for filter_data in filters:
            if filter_data['field'] == 'category':
                notes_list = notes_list.filter(
                    category__id__in=filter_data['value']
                )
            if filter_data['field'] == 'caption':
                notes_list = notes_list.filter(
                    caption__icontains=filter_data['value']
                )
            if filter_data['field'] == 'favorite':
                notes_list = notes_list.filter(
                    favorite=filter_data['value']
                )
            if filter_data['field'] == 'datetime':
                if filter_data['comparison'] == 'gt':
                    mydate = time.strptime(filter_data['value'],"%m/%d/%Y")
                    mydate = strftime('%Y-%m-%d', mydate)
                    notes_list = notes_list.filter(
                        datetime__gt=mydate
                    )
                if filter_data['comparison'] == 'lt':
                    mydate = time.strptime(filter_data['value'],"%m/%d/%Y")
                    mydate = strftime('%Y-%m-%d', mydate)
                    notes_list = notes_list.filter(
                        datetime__lt=mydate
                    )
                if filter_data['comparison'] == 'eq':
                    month, day, year = filter_data['value'].split('/')
                    notes_list = notes_list.filter(
                        datetime__day=day,
                        datetime__month=month,
                        datetime__year=year,
                    )

    notes_list = notes_list.values(
        'id',
        'caption',
        'datetime',
        'text',
        'favorite',
        'published',
        'uuid',
        'category__caption',
        'category__id'
    )

    note_count = len(notes_list)

    notes_list = notes_list[start:start+limit]

    #queryset в лист
    notes_list = list(notes_list)
    # for note_item in notes_list:
    #     if note_item['uuid']:
    #         note_item['uuid'] = 'http://localhost:8000/note/'+note_item['uuid']

    c.update({'root': notes_list, 'count': note_count})
    json_res = json.dumps(c, cls=DjangoJSONEncoder)  #Сериализация словаря

    return HttpResponse(json_res, mimetype='application/json')


@login_required(login_url='/accounts/login/')
def categories(request):
    c = {}

    category_count = models.Category.objects.all().count()

    queryset = models.Category.objects.all().values('id', 'caption')

    #queryset в лист
    categories_list = list(queryset)
    print u'::: \n'
    print 'categories_list %s \n' % len(categories_list)
    print categories_list

    print u'::: \n'

    c.update({'root': categories_list, 'count': category_count, 'success': True})
    json_res = json.dumps(c, cls=DjangoJSONEncoder)  #Сериализация словаря

    return HttpResponse(json_res, mimetype='application/json')


def note(request, note_uuid=0):
    from django.utils.timezone import utc
    c = {}

    from forms import EditForm

    if request.method == 'POST':
        print "REQUEST"
        print request.POST
        if request.POST['cmd'] == 'save':
            form = EditForm(request.POST)
            print "POST"
            print request.POST['category']
            if form.is_valid():
                cdata = form.cleaned_data
                # old_uuid = re.sub('http://note/','',cdata['uuid'])
                print cdata['published']
                old_uuid = cdata['uuid']
                note_uuid = ''
                if old_uuid == '':
                    if cdata['published']:
                        note_uuid = uuid.uuid4().hex
                else:
                    if not cdata['published']:
                        note_uuid = ''
                    else:
                        note_uuid = old_uuid

                category = models.Category.objects.get(caption=cdata['category'])

                if request.POST['id'] == '':
                    new_note = models.Note(
                        user=request.user,
                        datetime=datetime.datetime.utcnow().replace(tzinfo=utc),
                        caption=cdata['caption'],
                        text=cdata['text'],
                        category=category,
                        favorite=cdata['favorite'],
                        published=cdata['published'],
                        uuid=note_uuid,
                    )
                    new_note.save()
                    print new_note
                    return HttpResponse(
                        json.dumps({'success':True}),
                        mimetype='application/json'
                    )
                else:
                    print 'CDATA'
                    print cdata['category']
                    old_note = models.Note.objects.get(id=request.POST['id'])
                    old_note.datetime = datetime.datetime.utcnow().replace(tzinfo=utc)
                    old_note.caption = cdata['caption']
                    old_note.text = cdata['text']
                    old_note.category = category
                    old_note.favorite = cdata['favorite']
                    old_note.published = cdata['published']
                    old_note.uuid = note_uuid
                    old_note.save()
                    return HttpResponse(
                        json.dumps({'success':True}),
                        mimetype='application/json'
                    )
            else:
#                 c.update({'errors': form.errors})
                print form.errors.items()
                return HttpResponse(u'<br/>'.join(u'%s: %s' % (
                                    k, v[0])for k, v in form.errors.items()))
                c.update({
                    'success': False,
                    'errors': [(k, v[0]) for k, v in form.errors.items()]
                })

                json_res = json.dumps(c, cls=DjangoJSONEncoder)
                return HttpResponse(json_res, mimetype='application/json')

        if request.POST['cmd'] == 'delete':
            json_data = request.POST['json']
#            note_id = int(json_data['id'])
            requested_note = models.Note.objects.get(id=json_data)
            requested_note.delete()
#            print "JSON"
#            print request.POST['json']
#            print "DELETE"
            return HttpResponse(json.dumps({'success':True}), mimetype='application/json')
    else:
        # print note_uuid
#        note = models.Note.objects.get_(uuid = note_uuid)
        requested_note = get_object_or_404(models.Note, uuid=note_uuid)
        c.update({'note': requested_note})
        return render(request, 'note.html', c)
        # print "NOTE"
        # print note
    return HttpResponse(c)


def register(request):
    c = {}
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user = authenticate(username=request.POST['username'],
                                    password=request.POST['password1'])
            login(request, new_user)
            return HttpResponseRedirect("/")
        else:
            c.update({'errors': form.errors})
            print form.errors
            c.update({'form': form})
    else:
        form = UserCreationForm()
        c.update({'form': form})
    return render(request, "registration/register.html", c)