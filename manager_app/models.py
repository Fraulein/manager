# -*- coding: UTF-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    caption = models.CharField(max_length=1024, verbose_name=u'заголовок')

    def __unicode__(self):
        return self.caption

    class Meta:
        verbose_name = u'категория'
        verbose_name_plural = u'категории'
        ordering = ['caption']


class Note(models.Model):
    user = models.ForeignKey(User, verbose_name=u'пользователь')
    caption = models.CharField(max_length=1024, verbose_name=u'заголовок')
    text = models.TextField(verbose_name=u'текст')
    datetime = models.DateTimeField(verbose_name=u'дата и время')
    category = models.ForeignKey(Category, verbose_name=u'категория')
    favorite = models.BooleanField(default=False, verbose_name=u'избранная')
    published = models.BooleanField(default=False,verbose_name=u'опубликована')
    uuid = models.CharField(max_length=50, verbose_name=u'uuid')

    def __unicode__(self):
        return u'%s, %s' % (self.caption, self.datetime)

    def get_absolute_url(self):
        return u'/note/%s/' % self.uuid

    class Meta:
        verbose_name = u'заметка'
        verbose_name_plural = u'заметки'
        ordering = ['-datetime']