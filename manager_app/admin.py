# -*- coding: UTF-8 -*-
from django.contrib.admin import ModelAdmin, site
import models


class CategoryAdmin(ModelAdmin):
    pass


class NoteAdmin(ModelAdmin):
    save_as = True

site.register(models.Category, CategoryAdmin)
site.register(models.Note, NoteAdmin)
